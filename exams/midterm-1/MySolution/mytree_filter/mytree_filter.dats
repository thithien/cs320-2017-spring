(* ****** ****** *)

(*
** Midterm-1:
** mytree_filter: 10 points
*)

(* ****** ****** *)

staload
"./../../midterm-1.dats"

(* ****** ****** *)

(*
implement
mytree_filter(xs, p0) = ...
*)

(* ****** ****** *)

implement
main0 () =
{
//
val () =
println!
("Testing for mytree_filter")
//
(*
// Please write your own testing code
*)
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [mytree_filter.dats] *)
