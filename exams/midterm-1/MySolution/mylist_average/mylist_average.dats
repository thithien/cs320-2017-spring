(* ****** ****** *)

(*
** Midterm-1:
** mylist_average:
//
HX: 10 points
HX: 20 points if done solely based on combinators
HX: Please study mylist_foldleft and mylist_ifoldleft
    in ./../../mylib/mylist
//
*)

(* ****** ****** *)

staload
"./../../midterm-1.dats"

(* ****** ****** *)

(*
implement
mylist_average(xs) = ...
*)

(* ****** ****** *)

implement
main0 () =
{
//
val () =
println!
("Testing for mylist_average")
//
val xs =
g0ofg1
(
$list{double}
(1.0, 2.0, 3.0, 4.0, 5.0)
)
//
val ys = mylist_average(xs)
//
val () = println! ("ys = ", ys)
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [mylist_average.dats] *)
