(* ****** ****** *)

(*
** Midterm-1:
** mylist_submaxord:
//
// HX: 20 points
//
// Given a list xs of integers, the function
// [mylist_submaxord] returns the longest leftmost
// subsequence of xs that is ordered.
//
// If xs = (1, 3, 2, 4), then the result is (1, 3, 4)
//
// If xs = (4, 1, 2, 3, 8, 9, 5, 6, 7), then the result
// is (1, 2, 3, 5, 6, 7)
//
*)

(* ****** ****** *)

staload
"./../../midterm-1.dats"

(* ****** ****** *)

(*
implement
mylist_submaxord(xs) = ...
*)

(* ****** ****** *)

implement
main0 () =
{
//
val () =
println!
("Testing for mylist_submaxord")
//
val xs =
g0ofg1
(
$list{int}
(4, 1, 2, 3, 8, 9, 5, 6, 7)
)
//
val ys = mylist_submaxord(xs)
//
val () = println! ("ys = ", ys)
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [mylist_submaxord.dats] *)
