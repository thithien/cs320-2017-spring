(*
** Midterm-1:
** A_draw: 10 points
*)

(* ****** ****** *)
//
staload
"./../../midterm-1.dats"
//
(* ****** ****** *)

(*
implement
A_draw(n) = ...
*)

(* ****** ****** *)

implement
main0 () =
{
//
val () =
println! ("Testing for A_draw")
//
val () = A_draw(1)
val () = A_draw(2)
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [A_draw.dats] *)
