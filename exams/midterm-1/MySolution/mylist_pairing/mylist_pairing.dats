(* ****** ****** *)

(*
** Midterm-1:
** mylist_pairing: 10 points
*)

(* ****** ****** *)

staload
"./../../midterm-1.dats"

(* ****** ****** *)

(*
implement
mylist_pairing(xs) = ...
*)

(* ****** ****** *)

implement
main0 () =
{
//
val () =
println!
("Testing for mylist_pairing")
//
val xs =
g0ofg1
(
$list{int}
(1, 2, 3, 4, 5, 6)
)
//
val ys = mylist_pairing<int>(xs)
//
val () = println! ("ys = ", ys)
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [mylist_pairing.dats] *)
