(* ****** ****** *)

(*
** Midterm-1:
** mytree_minmaxHeight:
** 10 points
*)

(* ****** ****** *)

staload
"./../../midterm-1.dats"

(* ****** ****** *)

(*
implement
mytree_minmaxHeight(xs) = ...
*)

(* ****** ****** *)

implement
main0 () =
{
//
val () =
println!
("Testing for mytree_minmaxHeight")
//
(*
// Please write your own testing code
*)
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [mytree_minmaxHeight.dats] *)
