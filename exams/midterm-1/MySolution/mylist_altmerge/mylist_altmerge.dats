(* ****** ****** *)

(*
** Midterm-1:
** mylist_altmerge: 10 points
*)

(* ****** ****** *)

staload
"./../../midterm-1.dats"

(* ****** ****** *)

(*
implement
mylist_altmerge(xs) = ...
*)

(* ****** ****** *)

implement
main0 () =
{
//
val () =
println!
("Testing for mylist_altmerge")
//
val xs =
g0ofg1
(
$list{int}(1, 2, 3)
)
val ys =
g0ofg1
(
$list{int}(4, 5, 6, 7, 8)
)
//
val zs =
mylist_altmerge<int>(xs, ys)
//
val () = println! ("xs = ", xs)
val () = println! ("ys = ", ys)
val () = println! ("zs = ", zs)
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [mylist_altmerge.dats] *)
