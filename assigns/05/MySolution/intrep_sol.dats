(* ****** ****** *)
//
// HX:
// How to compile:
// patscc -DATS_MEMALLOC_LIBC \
// -o intrep_sol intrep_sol.dats
//
// How to test it:
// ./intrep_sol
//
(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#include "./../assign05.dats"

(* ****** ****** *)
//
fun
intrep2int
(
xs: list0(int)
) : int =
list0_foldright<int><int>
(
  xs, lam(x, n) => 2*n + x, 0
) (* list0_foldright *)
//
(* ****** ****** *)
//
// Please give your implementation as follows:
//
(* ****** ****** *)

#ifdef
MAIN_NONE
#then
#else

implement
main0() = () where
{
//
val () =
println!
(
"Hello from [intrep_sol]!"
) (* println! *)
//
val () =
println!("power(2, 10) = ", intrep2int(intrep_power(2, 10)))
val () =
println!("power(3, 10) = ", intrep2int(intrep_power(3, 10)))
//
} (* end of [main0] *)

#endif // end of #ifdef(MAIN_NONE)

(* ****** ****** *)

(* end of [intrep_sol.dats] *)
