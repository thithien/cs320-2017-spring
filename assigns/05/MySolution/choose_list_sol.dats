(* ****** ****** *)
//
// HX:
// How to compile:
// patscc -DATS_MEMALLOC_LIBC \
// -o choose_int_sol choose_int_sol.dats
//
// How to test it:
// ./choose_int_sol
//
(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#include "./../assign05.dats"

(* ****** ****** *)

#include
"$PATSHOMELOCS\
/effectivats-divideconquer/mylibies.hats"

(* ****** ****** *)

#staload DC = $DivideConquer

(* ****** ****** *)
//
assume
$DC.input_t0ype =
  (list0(elt), int, int)
//
assume
$DC.output_t0ype = list0(list0(elt))
//
(* ****** ****** *)
//
// Please give your implementation as follows:
//
(* ****** ****** *)

#ifdef
MAIN_NONE
#then
#else

assume
choose_list_elt = int

implement
main0() = () where
{
//
val () =
println!
(
"Hello from [choose_list_sol]!"
) (* println! *)
//
val xs =
$list{int}
(
  0, 1, 2, 3, 4
) (* val *)
val xs = g0ofg1(xs)
//
val xss = choose_list(xs, 3)
//
val out = stdout_ref
//
val ((*void*)) =
  fprint_listlist0_sep(out, xss, "\n", ", ")
//
val ((*void*)) = fprint_newline(out)
//
} (* end of [main0] *)

#endif // end of #ifdef(MAIN_NONE)

(* ****** ****** *)

(* end of [choose_list_sol.dats] *)
