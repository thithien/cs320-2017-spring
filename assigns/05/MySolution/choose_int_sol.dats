(* ****** ****** *)
//
// HX:
// How to compile:
// patscc -DATS_MEMALLOC_LIBC \
// -o choose_int_sol choose_int_sol.dats
//
// How to test it:
// ./choose_int_sol
//
(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#include "./../assign05.dats"

(* ****** ****** *)

#include
"$PATSHOMELOCS\
/effectivats-divideconquer/mylibies.hats"

(* ****** ****** *)

#staload DC = $DivideConquer

(* ****** ****** *)
//
assume
$DC.input_t0ype = (int, int)
//
assume $DC.output_t0ype = int
//
(* ****** ****** *)
//
// Please give your implementation as follows:
//
(* ****** ****** *)

#ifdef
MAIN_NONE
#then
#else

implement
main0() = () where
{
//
val () =
println!
(
"Hello from [choose_int_sol]!"
) (* println! *)
//
val () =
println!
("choose(5, 2) = ", choose_int(5, 2))
//
val () =
println!
("choose(10, 4) = ", choose_int(10, 4))
val () =
println!
("choose(10, 5) = ", choose_int(10, 5))
val () =
println!
("choose(10, 6) = ", choose_int(10, 6))
//
} (* end of [main0] *)

#endif // end of #ifdef(MAIN_NONE)

(* ****** ****** *)

(* end of [choose_int_sol.dats] *)
