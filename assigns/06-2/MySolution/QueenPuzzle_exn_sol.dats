(* ****** ****** *)
//
// Please study the QueenPuzzle
// example and modify it so that
// only the first solution found
// is returned.
//
(* ****** ****** *)

#include "./../QueenPuzzle_exn.dats"

(* ****** ****** *)
//
extern
fun
QueenPuzzle_helper:
  (list0(int)) -> int(*dummy*)
//
(* ****** ****** *)
//
exception FirstSolExn of list0(int)
//
(* ****** ****** *)
//
implement
QueenPuzzle_exn() =
try
let
val _0_ =
QueenPuzzle_helper(nil0)
in
  $raise NotFoundExn()
end with ~FirstSolExn(xs) => xs
//
(* ****** ****** *)

typedef input = $DC.input_t0ype
typedef output = $DC.output_t0ype

(* ****** ****** *)

assume $DC.input_t0ype = list0(int)
assume $DC.output_t0ype = int(*dummy*)

(* ****** ****** *)
//
// HX: Please give your implementation here
//
(* ****** ****** *)

(* end of [QueenPuzzle_exn_sol.dats] *)
