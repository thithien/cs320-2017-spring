(* ****** ****** *)
//
#include
"./../assign07.dats"
//
(* ****** ****** *)
//
// Please give your implementation here
//
(* ****** ****** *)

#ifdef
MAIN_NONE
#then
#else
implement
main0 () =
{
//
val () =
println!
(
  "Hello from [assign07_sol]!"
) (* val *)
//
val ln2_1M =
stream_nth_exn (stream_ln2(), 1000000)
val () = println! ("ln2_1M = ", ln2_1M)
//
val xys =
  intpair_enumerate()
val xys_10 = stream_take_exn(xys, 10)
val xys_10 = list0_of_list_vt(xys_10)
//
val () = println! ("xys_10 = ", xys_10)
//
val ln2 = stream_ln2()
val ln2 = EulerTrans(ln2)
val ln2 = EulerTrans(ln2)
val ln2 = EulerTrans(ln2)
val ln2 = EulerTrans(ln2)
val () = println! ("ln2_4_0 = ", stream_nth_exn(ln2, 0))
//
val () = println! ("Good news: Your code has passed initial testing!")
//
} (* end of [main0] *)
#endif // #ifdef

(* ****** ****** *)

(* end of [assign07_sol.dats] *)
